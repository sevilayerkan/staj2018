var app = angular.module('mainApp',['ngRoute']);

app.config(function($routeProvider){
	$routeProvider
	.when('/',{

	})
	.when('/about',{
		templateUrl: 'about.html';

	})
	.when('/contribute',{
		templateUrl: 'contribution.html';
	})
	.otherwise({
		redirectTo: '/'
	})
})