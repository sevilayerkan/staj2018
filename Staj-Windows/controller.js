var app = angular.module('mainApp',['ngRoute']);

app.config(function($routeProvider){
	$routeProvider
	.when('/',{

		templateUrl: 'page.html',
		//template: '<b>Template</b>',
	})
	.when('/helloUser', {
		templateUrl: 'greetings.html'
	})
	.otherwise({
		redirectTo: '/'
	});
});