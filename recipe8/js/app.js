var app = angular.module("MyApp", ['chieffancypants.loadingBar',"ngResource",'angular-loading-bar', 'ngAnimate', 'cfp.loadingBarInterceptor]',]);

app.config(function(cfpLoadingBarProvider,$httpProvider) {
  $httpProvider.responseInterceptors.push('myHttpInterceptor');
  cfpLoadingBarProvider.includeBar = true;
});

app.factory('myHttpInterceptor', function ($q, $window) {
  return function (promise) {
    return promise.then(function (response) {
      cfpLoadingBar.set();
      return response;
    }, function (response) {
      
      return $q.reject(response);
    });
  };
});

app.controller("MyCtrl", function($scope, $resource, $rootScope, $timeout, cfploadingBar) {

  $scope.resultsPerPage = 5;
  $scope.page = 1;
  $scope.searchTerm = "angular";

  $scope.twitter = $resource('http://search.twitter.com/search.json',
    { callback:'JSON_CALLBACK', page: $scope.page, rpp: $scope.resultsPerPage, q: $scope.searchTerm },
    { get: { method:'JSONP' } });

  $scope.load = function() {
    $scope.twitter.get(function(data) {
      ignoreLoadingBar: true
      $scope.tweets = data.results;
    });
  };

  $scope.start = function() {
    cfpLoadingBar.start();
  };

  $scope.complete = function () {
    cfpLoadingBar.complete();
  }

  // fake the initial load so first time users can see it right away:
  $scope.start();
  $scope.fakeIntro = true;
  $timeout(function() {
    $scope.complete();
    $scope.fakeIntro = false;
  }, 750);

});